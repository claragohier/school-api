const Exercises = require('../models/Exercises');

// création de la méthode permettant de créer un exercise
exports.createExercise = (req, res, next) => {
    const exercise = new Exercises({
        statement: req.body.statement,
        question: req.body.question,
        response: req.body.response
    });
    exercise.save().then(() => {
        res.status(200).json({
            message: "question enregistrée"
        });
    }).catch(error => {
        res.status(400).json(error);
    });
};

// création de la méthode permettant de récupérer les exercises
exports.getExercises = (req, res, next) => {
    Exercises.find().then(result => {
        res.status(200).json(result);
    }).catch(error => {
        res.status(400).json(error);
    });
};

// création de la méthode permettant de récupérer un exercise
exports.getExercise = (req, res, next) => {
    Exercises.findById(req.params.id).then(result => {
        res.status(200).json(result);
    }).catch(error => {
        res.status(400).json(error);
    });
};

// création de la méthode permettant de update un exercise
exports.updateExercise = (req, res, next) => {
    Exercises.findByIdAndUpdate(req.params.id, { $set: req.body }).then(() => {
        res.status(200).json({
            message: "l'exercice à été mis à jour"
        });
    }).catch(error => {
        res.status(400).json(error);
    });
};

// création de la méthode permettant de supprimer un exercise
exports.deleteExercise = (req,res,next) => {
    Exercises.findByIdAndDelete(req.params.id).then(() => {
        res.status(200).json({
            message: "exercice supprimé"
        });
    }).catch(error => {
        res.status(400).json(error);
    });
}