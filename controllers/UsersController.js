const Users = require('../models/Users');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

// création de la méthode pour créer un user
exports.createUser = (req, res, next) => {
    bcrypt.hash(req.body.password, 10).then(hash => {
        const user = new Users({
            username: req.body.username,
            password: hash
        });

        user.save().then(() => {
            res.status(200).json({
                message: "inscription réussie"
            });
        }).catch(error => {
            res.status(400).json(error);
        });

    }).catch(error => {
        res.status(400).json(error);
    });
};

// création de la méthode pour récupérer les users
exports.getUsers = (req, res, next) => {
    Users.find().then(result => {
        res.status(200).json(result);
    }).catch(error => {
        res.status(400).json(error);
    });
};

// création de la méthode pour récupérer un user
exports.getUser = (req, res, next) => {
    Users.findById(req.params.id).then(result => {
        res.status(200).json(result);
    }).catch(error => {
        res.status(400).json(error);
    });
};

// création de la méthode pour update un user
exports.updateUser = (req, res, next) => {
    Users.findByIdAndUpdate(req.params.id, { $set: req.body }).then(() => {
        res.status(200).json({
            message: "user mis à jour"
        });
    }).catch(error => {
        res.status(400).json(error);
    });
};

exports.deleteUser = (req, res, next) => {
    Users.findByIdAndDelete(req.params.id).then(() => {
        res.status(200).json({
            message: "user supprimé"
        });
    }).catch(error => {
        res.status(400).json(error);
    });
};

exports.login = (req, res, next) => {
    Users.findOne({ username: req.body.username }).then(user => {
        if (!user) {
            return res.status(401).json({
                message: "utilisateur non trouvé"
            });
        }
        bcrypt.compare(req.body.password, user.password).then(valid => {
            if (!valid) {
                return res.status(401).json({
                    message: "mot de passe incorect!"
                });
            }
            res.status(200).json({
                userId: user._id,
                username: user.username,
                token: jwt.sign(
                    { userId: user._id, username: user.username },
                    'RANDOM_TOKEN_SECRET',
                    { expiresIn: '24h' }
                )
            });
        }).catch(error => {
            res.status(400).json(error);
        }).catch(error => {
            res.status(400).json(error);
        })
    })
}

exports.verification = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwt.verify(token, 'RANDOM_TOKEN_SECRET');
        const userId = decodedToken.userId;
        if (req.body.userId && req.body.userId !== userId) {
            throw 'UserID invalide';
        } else {
            next();
        }
    } catch {
        res.status(401).json({
            error: new Error('Requête invalide!')
        });
    }
};