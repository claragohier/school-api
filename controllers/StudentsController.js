// import du model student
const Students = require('../models/Students');

// création de la méthode createStudent pour créer un élève
exports.createStudent = (req, res, next) => {
    const student = new Students({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        age: req.body.age,
        city: req.body.city
    });

    student.save().then(() => {
        res.status(200).json({
            message: "élève inscrit"
        });
    }).catch(error => {
        res.status(400).json(error);
    });
};

// création de la méthode getStudents pour récupérer tous les students
exports.getStudents = (req, res, next) => {
    Students.find().then(students => {
        res.status(200).json(students);
    }).catch(error => {
        res.status(400).json(error);
    });
}

// création de la méthode getStudent pour récupérer un student
exports.getStudent = (req, res, next) => {
    Students.findById(req.params.id).then(student => {
        res.status(200).json(student);
    }).catch(error => {
        res.status(400).json(error);
    });
};

// création de la méthode updateStudent pour mettre à jour l'élève
exports.updateStudent = (req, res, next) => {
    Students.findByIdAndUpdate(req.params.id, { $set: req.body }).then(() => {
        res.status(200).json({
            message: "élève mis à jour"
        });
    }).catch(error => {
        res.status(400).json(error);
    });
};

// création de la méthode deleteStudent pour supprimer un élève
exports.deleteStudent = (req, res, next) => {
    Students.findByIdAndDelete(req.params.id).then(() => {
        res.status(200).json({
            message: "élève supprimé"
        });
    }).catch(error => {
        res.status(400).json(error);
    });
}