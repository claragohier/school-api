// import d'express
const express = require('express');
// import du Router d'express
const router = express.Router();
// import du controller students
const studentsController = require('../controllers/StudentsController');

router.post("/", studentsController.createStudent);
router.get("/", studentsController.getStudents);
router.get("/:id", studentsController.getStudent);
router.put("/:id", studentsController.updateStudent);
router.delete("/:id", studentsController.deleteStudent);

module.exports = router;