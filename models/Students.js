const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const studentsSchema = Schema({
    firstname: { type: String, required: true },
    lastname: { type: String, required: true },
    age: { type: Number, required: true },
    city: { type: String, required: true }
})

module.exports = mongoose.model('Students', studentsSchema);