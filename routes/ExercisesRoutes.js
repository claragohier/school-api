const express = require('express');
const router = express.Router();
const exercisesController = require('../controllers/ExercisesController');

router.post('/', exercisesController.createExercise);
router.get('/', exercisesController.getExercises);
router.get('/:id', exercisesController.getExercise);
router.put('/:id', exercisesController.updateExercise);
router.delete('/:id', exercisesController.deleteExercise);

module.exports = router;