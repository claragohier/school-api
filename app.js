const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const bodyParser = require('body-parser');
// const cookieParser = require('cookie-parser');

const studentsRoute = require('./routes/StudentsRoutes');
const exercisesRoute = require('./routes/ExercisesRoutes');
const usersRoute = require('./routes/UsersRoutes');

const url = "mongodb+srv://claraG:clara123@cluster0.raoh7.mongodb.net/school?retryWrites=true&w=majority";

mongoose.connect(url, { useNewUrlParser: true }).then(() => {
    console.log('Connexion réussie');
}).catch(error => {
    console.log('Erreur' + error);
});

const app = express();


app.use(cors());
app.use(bodyParser.json());
// app.use(cookieParser.json());

app.use("/students", studentsRoute);
app.use("/exercises", exercisesRoute);
app.use("/users", usersRoute);

module.exports = app;