const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const exercisesSchema = Schema({
    statement: { type: String, required: true },
    question: { type: String, required: true },
    response: { type: String, required: true }
})

module.exports = mongoose.model('Exercises', exercisesSchema);